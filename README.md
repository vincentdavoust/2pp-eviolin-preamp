# Dual piezzo electric violin preamp

Dual piezo pickup electric violin preamp

Featuring :
- individual piezo gain control
- half-staged tunable 200Hz Hipass filter, to reduce bow noise
- half-staged tunable 8kHz Lopass filter, to reduce over harmonics
- Mains gain control
- attempt at analog reverb/distortion
